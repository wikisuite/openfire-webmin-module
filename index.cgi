#!/usr/bin/perl
require 'openfire-lib.pl';

my %openfire_config = &openfire_config_gather();

print &ui_print_header(undef, $text{'index_title'}, "", undef, 1, 1);

my @tabs = (
    ['main', &text('main')],
    ['logs', &text('logs')],
);

print &ui_tabs_start(\@tabs, 'section', $in{'section'} || $tabs[0]->[0], 1);

#
# tab main
#
print &ui_tabs_start_tab('section', 'main');
{
    #
    # Service status section
    #
    print &ui_subheading(
        "$text{'service_status'}: "
        . (
            openfire_service_is_up()
            ? '<strong class="text-success">' . &text('currently_running') . '</strong>'
            : '<strong class="text-danger">' . &text('currently_stopped') . '</strong>'
        )
    );
    print '<pre style="white-space: pre-wrap; background-color: black; color: white;">'
        . &openfire_service_status()
        . '</pre>';

    print &ui_form_start('./service.cgi', 'post');
    my @buttons = (
        ['start', $text{'start'},       "",  openfire_service_is_up() ],
        ['stop', $text{'edit_stopnow'}, "", !openfire_service_is_up() ],
        ['restart', $text{'restart'},   "", !openfire_service_is_up() ]
    );
    print &ui_form_end(\@buttons);

    local $openfire_pid = &openfire_service_get_pid();
    if ($openfire_pid) {
        #
        # Process details section
        #
        print &ui_subheading("Process details");
        print &ui_table_start();
        print &ui_table_row("PID", $openfire_pid, 2);
        if (my %info = &openfire_service_get_process_info()) {
            print &ui_table_row(
                "VMSize",
                sprintf("%d M", $info{'vmsize'} / (1024 * 1024)),
                2
            );
            print &ui_table_row(
                "VMRSS",
                sprintf("%d M", $info{'vmrss'}  / (1024 * 1024)),
                2
            );
        }
        print &ui_table_end();

        #
        # Network section
        #
        print &ui_subheading("Network info");
        print &ui_table_start();
        print &ui_table_row($text{'open_ports'}, '', 2);
        foreach my $val (&openfire_service_get_ports()) {
            print &ui_table_row(" ", $val, 2);
        }
        print &ui_table_end();
    }

    #
    # Openfire database section
    #
    print &ui_subheading($text{'database_config'});
    print &ui_table_start();
    foreach my $param (sort keys %{$openfire_config{'database'}})
    {
        my $val = $openfire_config{'database'}{$param};
        if (! ref $val) {
            print &ui_table_row($param, $val, 2);
        }
    }
    print &ui_table_end();
# /tab main
}
print &ui_tabs_end_tab();

#
# tab logs
#
print &ui_tabs_start_tab('section', 'logs');
{
    my @logs = &openfire_logs_list_files();
    print &ui_table_start();
    print &ui_table_row(
        "Select a log file",
        &ui_select(
            'log_file',
            $logs[0][0],
            \@logs,
            1, 0, 0, 0,
            'id="wof-log-selector" data-woflogs-el="wof-log-viewer" data-woflogs-source="./logs.cgi" '
        ),
        2
    );
    print &ui_table_end();
    print <<EOF
    <script type="text/javascript">
        jQuery(function() {
            const \$ = jQuery;
            const \$viewer = \$("#wof-log-viewer");
            const \$sel = \$("#wof-log-selector");
            const source = \$sel.data('woflogs-source');

            \$sel.on('change', function (evt) {
                const value = \$sel.val();
                const name = \$sel.prop('name');
                const data = { };
                data[ name ] = value;

                \$.ajax ({
                    "url": source,
                    "method": "POST",
                    "data": data,
                })
                .then(function(data) {
                    \$viewer.text(data);
                })
            });
        });
    </script>
EOF
;
    print '<pre id="wof-log-viewer" style="white-space: pre-wrap; background-color: black; color: white;">'
        . openfire_logs_read_file($logs[0][0])
        . '</pre>';

}
print &ui_tabs_end_tab();
# /tab logs


print &ui_tabs_end();