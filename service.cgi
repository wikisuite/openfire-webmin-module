#!/usr/bin/perl
use Data::Dumper
require 'openfire-lib.pl';
&ReadParse();

my $return = "";
if ($in{'restart'}) {
    $return .= "&" if $return;
    $return .= "restart_result=" . &openfire_service_restart();
}
else {
    if ($in{'stop'}) {
        $return .= "&" if $return;
        $return .= "stop_result=" . &openfire_service_stop();
    }
    if ($in{'start'}) {
        $return .= "&" if $return;
        $return .= "start_result=" . &openfire_service_start();
    }
}

&redirect("index.cgi?${return}");