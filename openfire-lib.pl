=head1 openfire-lib.pl

Functions for managing the Openfire webserver configuration file.

  foreign_require("openfire", "openfire-lib.pl");

=cut

BEGIN { push(@INC, ".."); };
use WebminCore;
use Data::Dumper;

init_config();

sub openfire_service_restart
{
  my $output = `systemctl restart openfire`;
  return $? == '0';
}

sub openfire_service_stop
{
  my $output = `systemctl stop openfire`;
  return $? == '0';
}

sub openfire_service_start
{
  my $output = `systemctl start openfire`;
  return $? == '0';
}

sub openfire_service_get_pid
{
    my $pid = -1;
    if ( -f '/var/run/openfire.pid' ) {
        open(FH, '<', '/var/run/openfire.pid');
        $pid = <FH>;
        $pid =~ s/\s+//;
        close(FH);
    }
    elsif ( -x "/etc/init.d/openfire" ) {
        my $output = `/etc/init.d/openfire status`;
        if ($output =~ m/openfire is running with pid (\d+)/) {
            $pid = $1;
        }
    }
    elsif ( my $out = `pgrep -o -f openfire/lib/startup.jar` ) {
        $out =~ s/\s+//;
        $pid = $out;
    }

    if ( $pid gt 0 && -f "/proc/${pid}/status" ) {
        return $pid
    }
    return -1;
}

sub openfire_service_get_ports
{
    $openfire_pid = &openfire_service_get_pid() if ! $openfire_pid;
    if ( ! -f "/proc/${openfire_pid}/net/tcp" ) {
        return ( );
    }

    my ($uid, $gid) = split(/\s+/, `stat -c '%u %g' /proc/${openfire_pid}`);
    my %ports = ();

    open(FH, "/proc/${openfire_pid}/net/tcp");
    my $line = <FH>;
    while ($line = <FH>) {
        $line =~ s/^\s+|\s+$//;
        my @parts = split(/\s+/, $line);
        my ($temp, $port) = split(/:/, $parts[1]);
        if ($temp eq '00000000' and $parts[7] eq $uid) {
            $port = hex("0x${port}");
            $ports{$port} = 1;
        }
    }
    close(FH);

    return sort keys %ports;
}

sub openfire_service_get_process_info
{
    $openfire_pid = &openfire_service_get_pid() if ! $openfire_pid;
    my $page_size = `getconf PAGESIZE`;
    my %info = ( );

    if ( $openfire_pid > 0 and -f "/proc/${openfire_pid}/statm" ) {
        open(FH, '<', "/proc/${openfire_pid}/statm");
        my ($vmsize, $vmrss) = split(/\s+/, <FH>);
        close(FH);

        $info{'vmsize'} = $vmsize * $page_size;
        $info{'vmrss'} = $vmrss * $page_size;
    }

    return %info;
}


sub openfire_service_status
{
  my $output = `systemctl status openfire`;
  return $output;
}

sub openfire_service_is_up
{
  my $output = `systemctl status openfire`;
  return $? == '0'
}

sub openfire_config_database_parse_dsn
{
    my ($str) = @_;

    my $regex = qr/
        ^
        (?:(\w+):)?          # (conn; ie: jdbc) or nothing
        (\w+)                # driver; ie: mysql
        :\/\/
        (?:(\w+):(\w+)@)?    # (user AND password) or nothing
        ((?:\w+)(?:\.\w+)*)  # hostname
        (?:\:(\d+))?         # (port) or nothing
        \/
        ([^\?]+)             # database name
        \?(.*)               # parameters
        $
    /x;

    if ($str =~ $regex) {
        my ($conn, $driver, $user, $pass, $host, $port, $dbname, $argstr) =
            ($1, $2, $3, $4, $5, $6, $7, $8);
        my %args = map { split(/=/, $_, 2) } split(/\&(?:amp;)?/, $argstr);
        return (
            'conn'   => $conn,
            'driver' => $driver,
            'user'   => $user,
            'pass'   => $pass,
            'host'   => $host,
            'port'   => $port,
            'dbname' => $dbname,
            'args'   => \%args
        );
    }
    return ( );
}

sub openfire_config_database_gather
{

    my ($el) = $openfire_xml->findnodes('/jive/database/defaultProvider/serverURL');
    my %database = openfire_config_database_parse_dsn($el->to_literal);

    ($el) = $openfire_xml->findnodes('/jive/database/defaultProvider/driver');
    $database{'driver'} = $el->to_literal;

    return %database;
}

sub openfire_config_gather
{
    eval "use XML::LibXML;";
    local %openfire_config;
    local $openfire_xml_file = '/etc/openfire/openfire.xml';
    local $openfire_xml = XML::LibXML->load_xml( location => $openfire_xml_file );

    my %database  = openfire_config_database_gather;
    $openfire_config{'database'} = \%database;

    return %openfire_config;
}

sub openfire_logs_list_files
{
    my $log_folder = '/var/log/openfire';
    opendir(my $dir, $log_folder);
    my @files = readdir($dir);
    closedir($dir);
    return sort grep(/(all|debug|error|info|warn).log/, @files);
}

sub openfire_logs_read_file
{
    my ($log_name, $lines) = @_;
    my $log_folder = '/var/log/openfire';
    my $file_path = "${log_folder}/${log_name}";
    $lines = 50 if ! $lines;

    if ( -f "$file_path" ) {
        open(FH, '<', $file_path);
        seek(FH, -1, 2);

        my $byte;
        my $count = 0;
        while (($count ne $lines) and (tell(FH) ne 0)) {
            seek(FH, -1, 1);
            read(FH, $byte, 1);

            if (ord($byte) eq 10) {
                $count++;
            }

            if ($count ne $lines) {
                seek(FH, -1, 1);
            }
        }
        local $/ = undef;
        my $content = <FH>;
        close(FH);
        return $content;
    }
    return undef;
}
