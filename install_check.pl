#!/usr/bin/perl

use warnings;

sub get_java_bin
{
    my $java;
    my $test;

    $test = `which java`;
    if ($test) {
        $java = $test;
    }

    if (exists $ENV{'JAVA_HOME'}) {
        $test = $ENV{'JAVA_HOME'} . '/bin/java';
        if (-x "$test") {
            $java = $test;
        }
    }

    if ($java) {
        $java =~ s/\s*$//;
        return $java;
    }
    return 0;
}

sub get_java_version
{
    my $java = get_java_bin;
    my $version = `$java -version 2>&1`;
    $version =~ /^\w+ version ['"]?(\d+(\.\d+)+(_\d+)?)['"]/;
    if ($version) {
        return $1;
    }
    return 0;
}

sub check_java_version
{
    my $version = get_java_version;
    if (! $version) {
        return 0;
    }
    $version =~ /(\d+)\.(\d+)\.(\d+)(_(\d+))?/;
    if ($1) {
        my $numeric = $1 . sprintf("%03d%03d", $2, $3);
        return $numeric >= 1008000;
    }
    return 0;
}


my $ok = check_java_version;
if ($ok) {
    exit 0;
}
exit 1;